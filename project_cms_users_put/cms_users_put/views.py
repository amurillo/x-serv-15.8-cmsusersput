from django.shortcuts import render, redirect
from .models import Content
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout


# ---------------------------------------------------------
# THIS CODE ONLY WORKS WITH PUT AND GET USING RESTER ADD-ON
# ---------------------------------------------------------

# Create your views here.
def index(request):
    content_list = Content.objects.all()
    context = {
        'content_list': content_list
    }

    return render(request, 'cms/index.html', context)


@csrf_exempt
def get_content(request, name):

    if request.user.is_authenticated:
        if request.method == "PUT":
            try:  # Content is in database
                content = Content.objects.get(name=name)  # Take content
            except Content.DoesNotExist:  # Content is not in database
                content = Content(name=name)  # Create new content
            content.value = request.body.decode('utf-8')  # Update value
            content.save()  # Save content

    else:
        response = "<p>You must log in to make changes</p>"
        context = {
            'response': response
        }

        return render(request, 'cms/schema.html', context)

    # Code below execute with both PUT and GET
    try:  # Content is in database
        content = Content.objects.get(name=name)  # Take content
        context = {
            'content': content
        }
    except Content.DoesNotExist:
        context = {
            'name': name
        }

    return render(request, 'cms/content.html', context)


def logout_view(request):
    logout(request)
    return redirect('/')
