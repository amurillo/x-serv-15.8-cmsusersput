from django.apps import AppConfig


class CmsUsersPutConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cms_users_put'
